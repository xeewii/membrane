package my.xeewi.membrane.data;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class PhotoInTemplate extends Photo {
    private final Frame mFrame;

    public PhotoInTemplate(Bitmap bitmap, ImageView imageView, Frame frame) {
        super(bitmap, imageView);

        this.mFrame = frame;
    }

    public Frame getFrame() {
        return mFrame;
    }
}

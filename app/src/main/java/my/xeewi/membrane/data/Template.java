package my.xeewi.membrane.data;

import android.graphics.Bitmap;

import my.xeewi.membrane.helpers.ImageHelper;

public class Template {
    private final PictureTemplate mPictureTemplate;

    public Template(PictureTemplate pictureTemplate) {
        this.mPictureTemplate = pictureTemplate;
    }

    public Bitmap getImageBitmap() {
        return ImageHelper.getInstance().getBitmapFromImageView(mPictureTemplate.getBackgroundImageView());
    }

    public PictureTemplate getPictureTemplate() {
        return mPictureTemplate;
    }
}
package my.xeewi.membrane.data;

import android.graphics.Bitmap;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Picture {
    private ImageView mBackgroundImageView;
    private List<Photo> mPhotoList = new ArrayList<>();
    private Photo mPhotoSelected;
    private final List<ImageView> mGlareImageViewList = new ArrayList<>();
    private final List<ImageView> mObjectImageViewList = new ArrayList<>();
    private Bitmap mExitBitmap;

    private Picture() {
    }

    public Photo getPhotoSelected() {
        return mPhotoSelected;
    }

    public void setPhotoSelected(Photo photoSelected) {
        this.mPhotoSelected = photoSelected;
    }

    public void addPhoto(Photo photo) {
        mPhotoList.add(photo);
    }

    public ImageView getBackgroundImageView() {
        return mBackgroundImageView;
    }

    public void setBackgroundImageView(ImageView backgroundImageView) {
        this.mBackgroundImageView = backgroundImageView;
    }

    public void setBackgroundBitmap(Bitmap bitmap){
        mBackgroundImageView.setImageBitmap(bitmap);
    }

    public Bitmap getExitBitmap() {
        return mExitBitmap;
    }

    public void setExitBitmap(Bitmap exitBitmap) {
        this.mExitBitmap = exitBitmap;
    }

    private static class PictureHolder{
        private final static Picture instance = new Picture();
    }

    public static Picture getInstance(){
        return Picture.PictureHolder.instance;
    }

    public List<Photo> getPhotoList(){
        return mPhotoList;
    }

    public List<ImageView> getGlareImageViewList(){
        return mGlareImageViewList;
    }

    public List<ImageView> getObjectImageViewList(){
        return mObjectImageViewList;
    }

    public void clear(){
        mPhotoList = new ArrayList<>();
    }

    public void addGlare(ImageView imageView){
        mGlareImageViewList.add(imageView);
    }

    public void addObject(ImageView imageView){
        mObjectImageViewList.add(imageView);
    }
}

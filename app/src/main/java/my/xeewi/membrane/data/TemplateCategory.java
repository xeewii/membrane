package my.xeewi.membrane.data;

import java.util.ArrayList;

public class TemplateCategory {
    private final String mNameCategory;
    private final ArrayList<Template> mTemplateList;

    public TemplateCategory(String nameCategory, ArrayList<Template> templateList){
        this.mNameCategory = nameCategory;
        this.mTemplateList = templateList;
    }

    public String getNameCategory() {
        return mNameCategory;
    }

    public ArrayList<Template> getTemplateList() {
        return mTemplateList;
    }
}

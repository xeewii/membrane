package my.xeewi.membrane.data;

public class Frame {
    // In pixels
    private final int mX;
    private final int mY;
    private final int mWidth;
    private final int mHeight;

    public Frame(int x, int y, int width, int height) {
        mX = x;
        mY = y;
        mWidth = width;
        mHeight = height;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }
}

package my.xeewi.membrane.data;

import android.graphics.Bitmap;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PictureTemplate {
    private final ImageView mBackgroundImageView;
    private final List<ForegroundImage> mForegroundImageList;
    private final Map<FrameLayout, Frame> mPhotoFrameMap;
    private final List<PhotoInTemplate> mPhotoList = new ArrayList<>();
    private Bitmap mExitBitmap;

    public PictureTemplate(ImageView backgroundImageView, List<ForegroundImage> foregroundImageList, Map<FrameLayout, Frame> photoFrameMap){
        this.mBackgroundImageView = backgroundImageView;
        this.mForegroundImageList = foregroundImageList;
        this.mPhotoFrameMap = photoFrameMap;
    }

    public ImageView getBackgroundImageView() {
        return mBackgroundImageView;
    }

    public void addPhoto(PhotoInTemplate photo) {
        mPhotoList.add(photo);
    }

    public List<PhotoInTemplate> getPhotoList(){
        return mPhotoList;
    }

    public Map<FrameLayout, Frame> getPhotoFrameMap() {
        return mPhotoFrameMap;
    }

    public List<ForegroundImage> getForegroundImageList() {
        return mForegroundImageList;
    }

    public Bitmap getExitBitmap() {
        return mExitBitmap;
    }

    public void setExitBitmap(Bitmap exitBitmap) {
        this.mExitBitmap = exitBitmap;
    }
}
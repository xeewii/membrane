package my.xeewi.membrane.data;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.widget.ImageView;

import my.xeewi.membrane.helpers.ImageHelper;

public class Photo {
    private Bitmap mOriginalBitmap;
    private final ImageView mImageView;

    private final ImageHelper mImageHelper = ImageHelper.getInstance();

    @SuppressLint("ClickableViewAccessibility")
    public Photo(Bitmap bitmap, ImageView imageView){
        mOriginalBitmap = bitmap;
        this.mImageView = imageView;
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public Bitmap getOriginalBitmap() {
        return mOriginalBitmap;
    }

    public Bitmap getBitmap() {
        return mImageHelper.getBitmapFromImageView(mImageView);
    }

    public void setBitmap(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }

    public void restoreOriginalBitmap(){
        mImageView.setImageBitmap(mOriginalBitmap);
    }

    public void saveNewBitmap(){
        mOriginalBitmap = mImageHelper.getBitmapFromImageView(mImageView);
        mImageView.setImageBitmap(mOriginalBitmap);
    }
}


package my.xeewi.membrane.data;

import android.widget.ImageView;

public class ForegroundImage {
    private final Frame mFrame;
    private final ImageView mImageView;

    public ForegroundImage(ImageView imageView, Frame frame) {
        this.mImageView = imageView;
        this.mFrame = frame;
    }

    public Frame getFrame() {
        return mFrame;
    }

    public ImageView getImageView() {
        return mImageView;
    }
}

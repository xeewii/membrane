package my.xeewi.membrane.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;

public class GalleryHelper {
    public static final int GALLERY_REQUEST = 1;

    private static class GalleryHelperHolder {
        private final static GalleryHelper instance = new GalleryHelper();
    }

    public static GalleryHelper getInstance() {
        return GalleryHelper.GalleryHelperHolder.instance;
    }

    private GalleryHelper(){
    }

    public void startChooseImage(Activity activity, int GALLERY_REQUEST){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        activity.startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
    }

    public void saveImageToGallery(Context context, Bitmap bitmap){
        MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "membrane_image", null);
    }
}

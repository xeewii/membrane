package my.xeewi.membrane.helpers;

public class ImageConstants {
    public static final int ImageWidth = 1200;
    public static final int ImageHeight = 1800;
    public static final int Scale = 2;
}

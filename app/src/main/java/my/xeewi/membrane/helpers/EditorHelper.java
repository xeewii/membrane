package my.xeewi.membrane.helpers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import my.xeewi.membrane.data.Photo;
import my.xeewi.membrane.data.Picture;
import my.xeewi.membrane.enums.EditorName;

public class EditorHelper {
    private EditorName mSelectedEditorName = EditorName.EXPOSITION;

    private static final int mMinValue = -100;
    private static final int mMaxValue = 100;
    private final static int mMaxValueForBar = Math.abs(mMinValue) + Math.abs(mMaxValue);

    private static class EditorHelperHolder {
        private final static EditorHelper instance = new EditorHelper();
    }

    public static EditorHelper getInstance() {
        return EditorHelper.EditorHelperHolder.instance;
    }

    private EditorHelper(){
    }

    public void editImage(int value){
        Photo photo = Picture.getInstance().getPhotoSelected();

        ColorMatrix colorMatrix = getColorMatrix(value);

        Bitmap newBitmap = getBitmapFromColorMatrix(colorMatrix, photo.getOriginalBitmap());

        photo.setBitmap(newBitmap);
    }

    private ColorMatrix getColorMatrix(int value){
        switch (mSelectedEditorName){
            case EXPOSITION: {
                return getColorMatrixExposition(value);
            }
            case SATURATION:{
                return getColorMatrixSaturation(value);
            }
            case CONTRAST:{
                return getColorMatrixContrast(value);
            }
            case TEMPERATURE:{
                return getColorMatrixTemperature(value);
            }
            case HUE:{
                return getColorMatrixHue(value);
            }
        }

        return new ColorMatrix();
    }

    public int getMaxValueForBar(){
        return mMaxValueForBar;
    }

    public int getDefaultValueForBar(){
        if(mSelectedEditorName == EditorName.SATURATION) {
            return mMaxValueForBar;
        }
        else {
            return mMaxValueForBar / 2;
        }
    }

    public void cancelChanges(){
        Photo photo = Picture.getInstance().getPhotoSelected();
        photo.restoreOriginalBitmap();
    }

    public void saveChanges(){
        Photo photo = Picture.getInstance().getPhotoSelected();
        photo.saveNewBitmap();
    }

    public Bitmap getBitmapFromColorMatrix(ColorMatrix colorMatrix, Bitmap bitmap){
        Bitmap bitmapResult = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        Paint paint = new Paint();

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
        paint.setColorFilter(filter);

        Canvas canvas = new Canvas(bitmapResult);
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return bitmapResult;
    }

    public void setSelectedEditorName(EditorName selectedEditorName) {
        mSelectedEditorName = selectedEditorName;
    }

    private ColorMatrix getColorMatrixExposition(float brightness) {
        brightness = brightness + mMinValue;

        return new ColorMatrix(new float[]
                {
                        1, 0, 0, 0, brightness,
                        0, 1, 0, 0, brightness,
                        0, 0, 1, 0, brightness,
                        0, 0, 0, 1, 0
                });
    }

    private ColorMatrix getColorMatrixSaturation(float saturation) {
        saturation /= 256;

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(saturation);

        return colorMatrix;
    }

    private ColorMatrix getColorMatrixTemperature(float temp) {
        temp = (temp + mMinValue)/mMaxValueForBar;

        return new ColorMatrix(new float[]
                {
                        1, 0, 0, temp, 0,
                        0, 1, 0, temp/2, 0,
                        0, 0, 1, temp/4, 0,
                        0, 0, 0, 1, 0
                });
    }

    public ColorMatrix getColorMatrixHue(float hue) {
        hue = (hue + mMinValue)/mMaxValueForBar;

        return new ColorMatrix(new float[]
                {
                        1, 0, 0, hue/2, 0,
                        0, 1, 0, hue, 0,
                        0, 0, 1, hue/2, 0,
                        0, 0, 0, 1, 0
                });
    }

    public ColorMatrix getColorMatrixContrast(float contrast) {
        contrast = (contrast + mMinValue)/mMaxValueForBar + 1;

        return new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, 0,
                        0, contrast, 0, 0, 0,
                        0, 0, contrast, 0, 0,
                        0, 0, 0, 1, 0
                });
    }

}

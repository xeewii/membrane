package my.xeewi.membrane.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import my.xeewi.membrane.R;
import my.xeewi.membrane.data.ForegroundImage;
import my.xeewi.membrane.data.Frame;
import my.xeewi.membrane.data.PictureTemplate;
import my.xeewi.membrane.data.Template;
import my.xeewi.membrane.data.TemplateCategory;

public class TemplatesHelper {
    private static XmlPullParser parser;

    private final ImageHelper mImageHelper = ImageHelper.getInstance();

    private static class TemplatesHelperHolder {
        private final static TemplatesHelper instance = new TemplatesHelper();
    }

    public static TemplatesHelper getInstance() {
        return TemplatesHelper.TemplatesHelperHolder.instance;
    }

    private TemplatesHelper(){

    }

    public List<TemplateCategory> createTemplates(Context context) {
        List<TemplateCategory> templateCategoryList = new ArrayList<>();

        try {
            parser = context.getResources().getXml(R.xml.templates);
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() == XmlPullParser.START_TAG) {
                    if (parser.getName().equals("category")) {
                        String nameCategory = parser.getAttributeValue(null, "name");

                        parser.nextTag();

                        if (parser.getName().equals("templates")) {
                            parser.nextTag();
                        } else {
                            throw new Exception("bad templates");
                        }

                        ArrayList<Template> templateList = new ArrayList<>();

                        while (!parser.getName().equals("templates")) {
                            if (parser.getEventType() == XmlPullParser.START_TAG) {
                                if (parser.getName().equals("template")) {
                                    parser.nextTag();

                                    ImageView backgroundImageView = createBackground(context);

                                    List<ForegroundImage> foregroundImageList = createForegrounds(context);

                                    Map<FrameLayout, Frame> photoFrameMap = createPhotos(context, backgroundImageView);

                                    PictureTemplate pictureTemplate = new PictureTemplate(backgroundImageView, foregroundImageList, photoFrameMap);

                                    Template template = new Template(pictureTemplate);

                                    templateList.add(template);

                                }
                            }

                            parser.nextTag();
                        }

                        TemplateCategory templateCategory = new TemplateCategory(nameCategory, templateList);

                        templateCategoryList.add(templateCategory);
                    }
                }
                parser.next();
            }
        } catch (Exception t) {
            Log.d("XML error", Objects.requireNonNull(t.getMessage()));
        }

        return templateCategoryList;
    }

    private ImageView createBackground(Context context) throws Exception {
        String backgroundName;
        if (parser.getName().equals("background")) {
            backgroundName = parser.getAttributeValue(null, "name");
        } else {
            throw new Exception("bad background");
        }

        ImageView backgroundImageView = new ImageView(context);
        Bitmap backgroundBitmap = mImageHelper.loadImage(context, backgroundName);
        backgroundImageView.setImageBitmap(backgroundBitmap);

        while (!(parser.getName().equals("background") && parser.getEventType() == XmlPullParser.END_TAG)) {
            parser.nextTag();
        }

        parser.nextTag();

        return backgroundImageView;
    }

    private List<ForegroundImage> createForegrounds(Context context) throws Exception {
        List<ForegroundImage> foregroundImageList = new ArrayList<>();

        if (parser.getName().equals("foregrounds")) {
            parser.nextTag();

            while (parser.getName().equals("foreground")) {
                if (parser.getEventType() == XmlPullParser.START_TAG) {
                    String foregroundName = parser.getAttributeValue(null, "name");
                    int x = Integer.parseInt(parser.getAttributeValue(null, "x"));
                    int y = Integer.parseInt(parser.getAttributeValue(null, "y"));
                    int width = Integer.parseInt(parser.getAttributeValue(null, "width"));
                    int height = Integer.parseInt(parser.getAttributeValue(null, "height"));

                    Frame foregroundFrame = new Frame(x, y, width, height);

                    ImageView foregroundImageView = new ImageView(context);
                    Bitmap foregroundBitmap = mImageHelper.loadImage(context, foregroundName);
                    foregroundImageView.setImageBitmap(foregroundBitmap);

                    ViewGroup.MarginLayoutParams layoutParams = new FrameLayout.LayoutParams((int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(foregroundFrame.getWidth())), (int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(foregroundFrame.getHeight())));
                    layoutParams.leftMargin = (int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(foregroundFrame.getX()));
                    layoutParams.topMargin = (int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(foregroundFrame.getY()));
                    foregroundImageView.setLayoutParams(layoutParams);

                    foregroundImageList.add(new ForegroundImage(foregroundImageView, foregroundFrame));
                }

                parser.nextTag();
            }
        } else {
            throw new Exception("bad foregrounds");
        }

        while (!(parser.getName().equals("foregrounds") && parser.getEventType() == XmlPullParser.END_TAG)) {
            parser.nextTag();
        }

        parser.nextTag();

        return foregroundImageList;
    }

    private Map<FrameLayout, Frame> createPhotos(Context context, ImageView backgroundImageView) throws Exception {
        Map<FrameLayout, Frame> photoFrameMap = new HashMap<>();
        if (parser.getName().equals("photos")) {
            parser.nextTag();

            while (parser.getName().equals("photo")) {
                if (parser.getEventType() == XmlPullParser.START_TAG) {
                    int x = Integer.parseInt(parser.getAttributeValue(null, "x"));
                    int y = Integer.parseInt(parser.getAttributeValue(null, "y"));
                    int width = Integer.parseInt(parser.getAttributeValue(null, "width"));
                    int height = Integer.parseInt(parser.getAttributeValue(null, "height"));

                    FrameLayout photoFrameLayout = new FrameLayout(context);

                    ViewGroup.MarginLayoutParams photoLayoutParams = new FrameLayout.LayoutParams((int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(width)), (int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(height)));
                    photoLayoutParams.leftMargin = (int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(x));
                    photoLayoutParams.topMargin = (int) DimensionUtils.pxToDp(context, DimensionUtils.getValidPx(y));
                    photoFrameLayout.setLayoutParams(photoLayoutParams);

                    initializePhotoFrameLayout(photoFrameLayout);

                    Frame frame = new Frame(x, y, width, height);

                    photoFrameMap.put(photoFrameLayout, frame);
                }

                parser.nextTag();
            }
        } else {
            throw new Exception("bad photos");
        }

        return photoFrameMap;
    }

    private void initializePhotoFrameLayout(FrameLayout photoFrameLayout){
        photoFrameLayout.setBackgroundResource(R.drawable.add_image_test);
        photoFrameLayout.getBackground().setAlpha(50);
    }
}
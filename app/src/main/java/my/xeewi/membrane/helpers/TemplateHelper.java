package my.xeewi.membrane.helpers;

import my.xeewi.membrane.data.PictureTemplate;

public class TemplateHelper {
    private PictureTemplate mPictureTemplate;

    private static class TemplateHelperHolder {
        private final static TemplateHelper instance = new TemplateHelper();
    }

    public static TemplateHelper getInstance() {
        return TemplateHelper.TemplateHelperHolder.instance;
    }

    private TemplateHelper(){

    }

    public PictureTemplate getPictureTemplate() {
        return mPictureTemplate;
    }

    public void setPictureTemplate(PictureTemplate pictureTemplate) {
        mPictureTemplate = pictureTemplate;
    }
}

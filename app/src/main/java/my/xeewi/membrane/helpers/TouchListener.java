package my.xeewi.membrane.helpers;

import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class TouchListener implements View.OnTouchListener {
    FrameLayout.LayoutParams layoutParams;

    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;

    float dx = 0, dy = 0, x = 0, y = 0;
    float angle = 0;

    private int mode = NONE;
    private float oldDist = 1f;
    private float d = 0f;

    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(View v, MotionEvent event) {
        final ImageView view = (ImageView) v;

        ((BitmapDrawable) view.getDrawable()).setAntiAlias(true);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                dx = event.getRawX() - layoutParams.leftMargin;
                dy = event.getRawY() - layoutParams.topMargin;
                mode = DRAG;
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    mode = ZOOM;
                }

                d = rotation(event);

                break;
            case MotionEvent.ACTION_UP:

                break;

            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;

                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {

                    x = event.getRawX();
                    y = event.getRawY();

                    layoutParams.leftMargin = (int) (x - dx);
                    layoutParams.topMargin = (int) (y - dy);

                    layoutParams.rightMargin = 0;
                    layoutParams.bottomMargin = 0;
                    layoutParams.rightMargin = layoutParams.leftMargin + (5 * layoutParams.width);
                    layoutParams.bottomMargin = layoutParams.topMargin + (10 * layoutParams.height);

                    view.setLayoutParams(layoutParams);

                } else if (mode == ZOOM) {

                    if (event.getPointerCount() == 2) {

                        float newRot = rotation(event);
                        angle = newRot - d;

                        x = event.getRawX();
                        y = event.getRawY();

                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            float scale = newDist / oldDist * view.getScaleX();
                            if (scale > 0.05) {
                                view.setScaleX(scale);
                                view.setScaleY(scale);
                            }
                        }

                        view.animate().rotationBy(angle).setDuration(0).setInterpolator(new LinearInterpolator()).start();

                        x = event.getRawX();
                        y = event.getRawY();

                        layoutParams.leftMargin = (int) ((x - dx));
                        layoutParams.topMargin = (int) ((y - dy));

                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = 0;
                        layoutParams.rightMargin = layoutParams.leftMargin + (5 * layoutParams.width);
                        layoutParams.bottomMargin = layoutParams.topMargin + (10 * layoutParams.height);

                        view.setLayoutParams(layoutParams);
                    }
                }
                break;
        }

        return true;
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }
}
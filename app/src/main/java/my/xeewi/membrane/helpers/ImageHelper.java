package my.xeewi.membrane.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import my.xeewi.membrane.data.ForegroundImage;
import my.xeewi.membrane.data.Frame;
import my.xeewi.membrane.data.Photo;
import my.xeewi.membrane.data.PhotoInTemplate;
import my.xeewi.membrane.data.Picture;
import my.xeewi.membrane.data.PictureTemplate;

public class ImageHelper {
    private static final int stepForResizePhoto = 100;

    private static class ImageHelperHolder {
        private final static ImageHelper instance = new ImageHelper();
    }

    public static ImageHelper getInstance() {
        return ImageHelper.ImageHelperHolder.instance;
    }

    private ImageHelper(){

    }

    public Bitmap combineImages(List<Photo> photoList) {
        Picture picture = Picture.getInstance();

        ImageView backgroundImageView = picture.getBackgroundImageView();
        Bitmap backgroundBitmap = getBitmapFromImageView(backgroundImageView);

        int widthBackgroundBitmap = backgroundBitmap.getWidth();
        int heightBackgroundBitmap = backgroundBitmap.getHeight();

        Bitmap temp = Bitmap.createBitmap(widthBackgroundBitmap, heightBackgroundBitmap, Bitmap.Config.ARGB_8888);

        int widthBackgroundImageView = backgroundImageView.getWidth();
        int heightBackgroundImageView = backgroundImageView.getHeight();

        float scaleXBackground = (float) widthBackgroundImageView / widthBackgroundBitmap;
        float scaleYBackground = (float) heightBackgroundImageView / heightBackgroundBitmap;

        Canvas canvas = new Canvas(temp);
        canvas.drawBitmap(backgroundBitmap, 0, 0, null);

        for (Photo photo : photoList) {
            Bitmap bitmap = photo.getBitmap();
            ImageView photoImageView = photo.getImageView();

            float scaleXPhotoImageView = photoImageView.getScaleX();
            float scaleYPhotoImageView = photoImageView.getScaleY();

            float scaleX = scaleXPhotoImageView / scaleXBackground;
            float scaleY = scaleYPhotoImageView / scaleYBackground;

            int[] values = new int[2];
            photoImageView.getLocationOnScreen(values);

            int[] valuesBack = new int[2];
            backgroundImageView.getLocationOnScreen(valuesBack);

            float xDiff = values[0] - valuesBack[0];
            float yDiff = values[1] - valuesBack[1];

            Matrix matrix = new Matrix();
            matrix.setRotate(photoImageView.getRotation());
            matrix.postTranslate(xDiff / scaleXPhotoImageView, yDiff / scaleYPhotoImageView);
            matrix.postScale(scaleX, scaleY);

            canvas.drawBitmap(bitmap, matrix, null);
        }

        for(ImageView imageView : picture.getGlareImageViewList()){
            Bitmap bitmap = getBitmapFromImageView(imageView);

            Matrix matrix = getMatrix(imageView, backgroundImageView, scaleXBackground, scaleYBackground);

            Paint transparentPaint = new Paint();
            transparentPaint.setAlpha((int) (imageView.getAlpha() * 100));

            canvas.drawBitmap(bitmap, matrix, transparentPaint);
        }

        for(ImageView imageView : picture.getObjectImageViewList()){
            Bitmap bitmap = getBitmapFromImageView(imageView);

            Matrix matrix = getMatrix(imageView, backgroundImageView, scaleXBackground, scaleYBackground);

            canvas.drawBitmap(bitmap, matrix, null);
        }

        picture.setExitBitmap(temp);

        return temp;
    }

    private Matrix getMatrix(ImageView imageView, ImageView backgroundImageView, float scaleXBackground, float scaleYBackground){
        float scaleXPhotoImageView = imageView.getScaleX();
        float scaleYPhotoImageView = imageView.getScaleY();

        float scaleX = scaleXPhotoImageView / scaleXBackground;
        float scaleY = scaleYPhotoImageView / scaleYBackground;

        int[] values = new int[2];
        imageView.getLocationOnScreen(values);

        int[] valuesBack = new int[2];
        backgroundImageView.getLocationOnScreen(valuesBack);

        float xDiff = values[0] - valuesBack[0];
        float yDiff = values[1] - valuesBack[1];

        Matrix matrix = new Matrix();
        matrix.setRotate(imageView.getRotation());
        matrix.postTranslate(xDiff / scaleXPhotoImageView, yDiff / scaleYPhotoImageView);
        matrix.postScale(scaleX, scaleY);

        return matrix;
    }

    public Bitmap combineImages(List<PhotoInTemplate> photoList, List<ForegroundImage> foregroundImageList) {
        PictureTemplate pictureTemplate = TemplateHelper.getInstance().getPictureTemplate();
        Bitmap backgroundBitmap = ((BitmapDrawable) pictureTemplate.getBackgroundImageView().getDrawable()).getBitmap();
        ImageView backgroundImageView = pictureTemplate.getBackgroundImageView();

        int widthBackgroundBitmap = backgroundBitmap.getWidth();
        int heightBackgroundBitmap = backgroundBitmap.getHeight();

        Bitmap temp = Bitmap.createBitmap(widthBackgroundBitmap, heightBackgroundBitmap, Bitmap.Config.ARGB_8888);

        int widthBackgroundImageView = backgroundImageView.getWidth();
        int heightBackgroundImageView = backgroundImageView.getHeight();

        float scaleXBackground = (float) widthBackgroundImageView / widthBackgroundBitmap;
        float scaleYBackground = (float) heightBackgroundImageView / heightBackgroundBitmap;

        Canvas canvas = new Canvas(temp);
        canvas.drawBitmap(backgroundBitmap, 0, 0, null);

        for (PhotoInTemplate photo : photoList) {
            Bitmap bitmap = photo.getBitmap();
            ImageView photoImageView = photo.getImageView();

            float scaleXPhotoImageView = photoImageView.getScaleX();
            float scaleYPhotoImageView = photoImageView.getScaleY();

            float scaleX = scaleXPhotoImageView / scaleXBackground;
            float scaleY = scaleYPhotoImageView / scaleYBackground;

            int[] values = new int[2];
            photoImageView.getLocationOnScreen(values);

            int[] valuesBack = new int[2];
            backgroundImageView.getLocationOnScreen(valuesBack);

            float xDiff = values[0] - valuesBack[0];
            float yDiff = values[1] - valuesBack[1];

            Matrix matrix = new Matrix();
            matrix.setRotate(photoImageView.getRotation());
            matrix.postTranslate(xDiff / scaleXPhotoImageView, yDiff / scaleYPhotoImageView);
            matrix.postScale(scaleX, scaleY);

            Bitmap mask = Bitmap.createBitmap(1200, 1800, Bitmap.Config.ARGB_8888);
            int widthFrame = photo.getFrame().getWidth();
            int heightFrame = photo.getFrame().getHeight();
            int[] colors = new int[widthFrame*heightFrame];
            Arrays.fill(colors, 0, widthFrame*heightFrame, Color.RED);
            mask.setPixels(colors, 0, widthFrame, photo.getFrame().getX(), photo.getFrame().getY(), widthFrame, heightFrame);

            Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas tempCanvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            tempCanvas.drawBitmap(bitmap, matrix, null);
            tempCanvas.drawBitmap(mask, 0, 0, paint);
            paint.setXfermode(null);

            //Draw result after performing masking
            canvas.drawBitmap(result, 0, 0, new Paint());
        }

        drawForeground(canvas, foregroundImageList);

        pictureTemplate.setExitBitmap(temp);

        return temp;
    }

    @SuppressLint("ClickableViewAccessibility")
    public Photo createPhotoForPicture(Context context, FrameLayout frameLayout, Bitmap bitmap){
        ImageView imageView = new ImageView(context);

        imageView.setOnTouchListener(new TouchListener());

        frameLayout.addView(imageView);

        imageView.setImageBitmap(bitmap);

        assert bitmap != null;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int[] size = new int[]{height, width};

        getValidSizePhoto(size);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);
        imageView.setLayoutParams(layoutParams);

        float scale = 1f*size[1]/width;
        imageView.setScaleX(scale);
        imageView.setScaleY(scale);

        imageView.setPivotX(0);
        imageView.setPivotY(0);

        return new Photo(bitmap, imageView);
    }

    @SuppressLint("ClickableViewAccessibility")
    public PhotoInTemplate createPhotoInTemplateForPicture(Context context, Bitmap bitmap, FrameLayout frameLayout, Frame frame){
        ImageView imageView = new ImageView(context);

        imageView.setOnTouchListener(new TouchListener());

        frameLayout.addView(imageView);

        imageView.setImageBitmap(bitmap);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
        imageView.setLayoutParams(layoutParams);

        float scale = 1f*frame.getWidth()/bitmap.getWidth();
        imageView.setScaleX(scale);
        imageView.setScaleY(scale);

        imageView.setPivotX(0);
        imageView.setPivotY(0);

        return new PhotoInTemplate(bitmap, imageView, frame);
    }

    private void drawForeground(Canvas canvas, List<ForegroundImage> foregroundImageList){
        for(ForegroundImage foregroundImage: foregroundImageList){
            Bitmap bitmap = ((BitmapDrawable)foregroundImage.getImageView().getDrawable()).getBitmap();
            Frame frame = foregroundImage.getFrame();
            Bitmap.createScaledBitmap(bitmap, frame.getWidth(), frame.getHeight(), false);
            canvas.drawBitmap(bitmap, frame.getX(), frame.getY(), new Paint());
        }
    }

    public Bitmap getBitmapFromImageView(ImageView imageView){
        return ((BitmapDrawable)imageView.getDrawable()).getBitmap();
    }

    public Bitmap loadImage(Context context, String name){
        InputStream ims = null;
        try {
            ims = context.getAssets().open(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return BitmapFactory.decodeStream(ims);
    }

    public List<Bitmap> loadFolder(Context context, String path){
        List<Bitmap> bitmapList = new ArrayList<>();
        AssetManager assetManager = context.getAssets();
        try {
            String[] files = assetManager.list(path);
            assert files != null;
            for (String fileName: files){
                if(!fileName.equals("preview.png")) {
                    InputStream ims = assetManager.open(path + "/" + fileName);
                    Bitmap bitmap = BitmapFactory.decodeStream(ims);
                    bitmapList.add(bitmap);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmapList;
    }

    private void getValidSizePhoto(int[] sizes){
        while (sizes[0] > ImageConstants.ImageHeight || sizes[1] > ImageConstants.ImageWidth) {
            sizes[0] -= stepForResizePhoto;
            sizes[1] -= stepForResizePhoto;
        }

        while (sizes[0] < ImageConstants.ImageHeight - stepForResizePhoto && sizes[1] < ImageConstants.ImageWidth - stepForResizePhoto) {
            sizes[0] += stepForResizePhoto;
            sizes[1] += stepForResizePhoto;
        }
    }

    public ImageView createGlareImageView(Bitmap bitmap, FrameLayout frameLayout, Context context){
        ImageView imageView = createImageView(bitmap, frameLayout, context);

        imageView.setAlpha(0.5f);

        return imageView;
    }

    public ImageView createObjectImageView(Bitmap bitmap, FrameLayout frameLayout, Context context){
        return createImageView(bitmap, frameLayout, context);
    }

    @SuppressLint("ClickableViewAccessibility")
    public ImageView createImageView(Bitmap bitmap, FrameLayout frameLayout, Context context){
        ImageView imageView = new ImageView(context);

        imageView.setOnTouchListener(new TouchListener());

        frameLayout.addView(imageView);

        imageView.setImageBitmap(bitmap);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
        imageView.setLayoutParams(layoutParams);

        return imageView;
    }
}
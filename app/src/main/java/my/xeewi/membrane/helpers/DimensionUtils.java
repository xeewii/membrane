package my.xeewi.membrane.helpers;

import android.content.Context;
import android.util.DisplayMetrics;

public final class DimensionUtils {
    private static boolean isInitialised = false;
    private static float pixelsPerOneDp;

    private DimensionUtils() {
        throw new AssertionError();
    }

    private static void initialise(Context view) {
        pixelsPerOneDp = view.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT;
        isInitialised = true;
    }

    public static float pxToDp(Context view, float px) {
        if (!isInitialised) {
            initialise(view);
        }

        return px / pixelsPerOneDp;
    }

    public static float dpToPx(Context view, float dp) {
        if (!isInitialised) {
            initialise(view);
        }

        return dp * pixelsPerOneDp;
    }

    public static float getValidPx(float px){
        return px*ImageConstants.Scale;
    }
}

package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import java.util.ArrayList;
import java.util.List;

import my.xeewi.membrane.R;
import my.xeewi.membrane.enums.MainMenuName;

public class MainMenu {
    private final List<MainMenuItem> itemList = new ArrayList<>();

    public MainMenu(){
        addItem(R.drawable.editor_off, R.drawable.editor_on, MainMenuName.EDITOR);
        addItem(R.drawable.back_off, R.drawable.back_on, MainMenuName.BACK_CATEGORY);
        addItem(R.drawable.objects_off, R.drawable.objects_on, MainMenuName.OBJECTS_CATEGORY);
        addItem(R.drawable.glare_off, R.drawable.glare_on, MainMenuName.GLARE_CATEGORY);
        addItem(R.drawable.skins_off, R.drawable.skins_on, MainMenuName.SKINS);
        addItem(R.drawable.filters_off, R.drawable.filters_on, MainMenuName.FILTERS);
        addItem(R.drawable.effects_off, R.drawable.effects_on, MainMenuName.EFFECTS);
        addItem(R.drawable.text_off, R.drawable.text_on, MainMenuName.TEXT);
    }

    private void addItem(int imageOff, int imageOn, MainMenuName name){
        addItem(new MainMenuItem(imageOff, imageOn, name));
    }

    private void addItem(MainMenuItem item){
        itemList.add(item);
    }

    public List<MainMenuItem> getItemList(){
        return itemList;
    }
}

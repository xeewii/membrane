package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import my.xeewi.membrane.R;

public abstract class AMenuViewHolder extends RecyclerView.ViewHolder {
    protected LinearLayout mLinearLayout;
    protected ImageView mImageView;

    public AMenuViewHolder(View view) {
        super(view);

        mImageView = view.findViewById(R.id.ivPreview);
        mLinearLayout = view.findViewById(R.id.linear_layout);
    }

    public abstract void onBind(IMenuItem menuItem);
}


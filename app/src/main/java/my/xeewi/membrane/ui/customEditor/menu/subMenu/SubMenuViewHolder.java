package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.graphics.Bitmap;
import android.view.View;
import my.xeewi.membrane.R;
import my.xeewi.membrane.ui.customEditor.menu.editor.EditorMenuItem;

public class SubMenuViewHolder extends AMenuViewHolder {
    private final SubMenuContract.View mView;

    public SubMenuViewHolder(View viewHolder, SubMenuContract.View view) {
        super(viewHolder);

        mView = view;

        mImageView = viewHolder.findViewById(R.id.ivPreview);
        mLinearLayout = viewHolder.findViewById(R.id.linear_layout);
    }

    public void onBind(IMenuItem subMenuItem){
        Bitmap bitmap = subMenuItem.getPreviewBitmap();
        mImageView.setImageBitmap(bitmap);

        mLinearLayout.setOnClickListener(createClickListener(subMenuItem, bitmap));
    }

    private View.OnClickListener createClickListener(IMenuItem subMenuItem, Bitmap bitmap){
        switch (subMenuItem.getMainMenuName()){
            case BACK: {
                return mView.getBackClickListener(bitmap);
            }
            case GLARE:{
                return mView.getGlareClickListener(bitmap);
            }
            case OBJECTS:{
                return mView.getObjectClickListener(bitmap);
            }
            case EDITOR: {
                return mView.getEditorClickListener((EditorMenuItem) subMenuItem);
            }
        }

        return null;
    }
}
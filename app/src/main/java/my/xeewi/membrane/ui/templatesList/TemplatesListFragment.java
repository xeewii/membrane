package my.xeewi.membrane.ui.templatesList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseFragment;
import my.xeewi.membrane.data.Template;
import my.xeewi.membrane.data.TemplateCategory;
import my.xeewi.membrane.ui.templatesList.list.TemplateCategoryListAdapter;

public final class TemplatesListFragment extends BaseFragment implements TemplatesListContract.View {

    private TemplatesListContract.Presenter mPresenter;

    public TemplatesListFragment() {
    }

    public static TemplatesListFragment newInstance() {
        return new TemplatesListFragment();
    }

    @Override
    public void setPresenter(TemplatesListContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_templates_list_layout, container, false);
    }

    @Override
    public void setTemplates(List<TemplateCategory> templateCategoryList){
        RecyclerView templateCategoryRecyclerView = Objects.requireNonNull(getView()).findViewById(R.id.rvTemplateCategoryList);
        templateCategoryRecyclerView.setHasFixedSize(true);
        TemplateCategoryListAdapter adapter = new TemplateCategoryListAdapter(this, templateCategoryList);
        templateCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        templateCategoryRecyclerView.setAdapter(adapter);
    }

    @Override
    public void loadTemplate(Template template) {
       mPresenter.loadTemplateEditor(template.getPictureTemplate());
    }

    @Override
    public LinearLayoutManager getHorizontalLinearLayoutManager() {
        return new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
    }
}

package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import my.xeewi.membrane.common.BaseContract;

public interface MainMenuContract {

    interface View extends BaseContract.View<Presenter> {
        void updateMainMenu(MainMenuListAdapter adapter);

        void loadSubMenu(MainMenuItem mainMenuItem);
    }

    interface Presenter extends BaseContract.Presenter {
        void updateMenuController(MainMenuItem mainMenuItem);
    }
}

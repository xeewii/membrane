package my.xeewi.membrane.ui.templatesList.list;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import my.xeewi.membrane.R;
import my.xeewi.membrane.data.TemplateCategory;
import my.xeewi.membrane.ui.templatesList.TemplatesListContract;

public class TemplateCategoryListAdapter extends RecyclerView.Adapter<TemplateCategoryViewHolder> {
    private final List<TemplateCategory> mTemplateCategoryList;
    private final TemplatesListContract.View mView;

    public TemplateCategoryListAdapter(TemplatesListContract.View view, List<TemplateCategory> templateCategoryList) {
        this.mTemplateCategoryList = templateCategoryList;
        this.mView = view;
    }

    @NonNull
    @Override
    public TemplateCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_category_list_item, null);

        return new TemplateCategoryViewHolder(view, mView);
    }

    @Override
    public void onBindViewHolder(TemplateCategoryViewHolder templateCategoryViewHolder, int i) {
        templateCategoryViewHolder.onBind(mTemplateCategoryList.get(i));
    }

    @Override
    public int getItemCount() {
        return (null != mTemplateCategoryList ? mTemplateCategoryList.size() : 0);
    }
}
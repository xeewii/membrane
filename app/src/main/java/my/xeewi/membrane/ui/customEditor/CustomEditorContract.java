package my.xeewi.membrane.ui.customEditor;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import my.xeewi.membrane.common.BaseContract;
import my.xeewi.membrane.ui.customEditor.menu.mainMenu.MainMenuPresenter;

public interface CustomEditorContract {

    interface View extends BaseContract.View<Presenter> {
        void setPictureFrameLayout(ViewGroup.LayoutParams layoutParams);

        void setBackground(ImageView imageView);

        FrameLayout getFrameLayoutForPhotos();
    }

    interface Presenter extends BaseContract.Presenter {
        void setMainMenuPresenter(MainMenuPresenter mainMenuPresenter);

        void addPhotoToPicture(Bitmap bitmap);

        void savePhoto();

        void setBackgroundImageView(ImageView imageView);
    }
}

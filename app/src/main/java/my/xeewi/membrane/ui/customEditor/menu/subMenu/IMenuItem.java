package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.graphics.Bitmap;

import my.xeewi.membrane.enums.MainMenuName;

public interface IMenuItem {
    Bitmap getPreviewBitmap();

    String getName();

    MainMenuName getMainMenuName();
}

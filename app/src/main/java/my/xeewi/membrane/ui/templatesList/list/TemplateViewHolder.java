package my.xeewi.membrane.ui.templatesList.list;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import my.xeewi.membrane.R;
import my.xeewi.membrane.data.Template;
import my.xeewi.membrane.ui.templatesList.TemplatesListContract;

public class TemplateViewHolder extends RecyclerView.ViewHolder {
    private final ImageView mItemImage;
    private final LinearLayout mLinearLayout;
    private final TemplatesListContract.View mView;

    public TemplateViewHolder(View viewHolder, TemplatesListContract.View view) {
        super(viewHolder);

        mView = view;
        mItemImage = viewHolder.findViewById(R.id.ivTemplate);
        mLinearLayout = viewHolder.findViewById(R.id.llTemplate);
    }

    public void onBind(Template template){
        mItemImage.setImageBitmap(template.getImageBitmap());

        mLinearLayout.setOnClickListener(createOnClickListener(template));
    }

    View.OnClickListener createOnClickListener(Template template){
        return v -> mView.loadTemplate(template);
    }
}
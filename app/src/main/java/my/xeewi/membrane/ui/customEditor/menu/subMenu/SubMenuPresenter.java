package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import my.xeewi.membrane.common.BasePresenter;
import my.xeewi.membrane.data.Picture;
import my.xeewi.membrane.helpers.ImageHelper;
import my.xeewi.membrane.ui.customEditor.menu.MenuController;

public class SubMenuPresenter extends BasePresenter implements SubMenuContract.Presenter {

    private final SubMenuContract.View mView;

    private final SubMenu mSubMenu;

    private final Context mContext;

    private final Picture mPicture = Picture.getInstance();

    private final MenuController mMenuController = MenuController.getInstance();

    private final ImageHelper mImageHelper = ImageHelper.getInstance();

    public SubMenuPresenter(@NonNull Context context, @NonNull SubMenuContract.View view) {
        this.mView = view;
        this.mContext = context;
        this.mView.setPresenter(this);

        mSubMenu = new SubMenu(context);
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void updateItemList() {
        SubMenuListAdapter adapter = new SubMenuListAdapter(mView, mSubMenu.getItemList());

        adapter.notifyDataSetChanged();

        mView.updateSubMenu(adapter);
    }

    @Override
    public void updateMenuController(CategorySubMenuItem categorySubMenuItem) {
        mMenuController.setSelectedMenuName(categorySubMenuItem.getMainMenuName());
        mMenuController.setPath((categorySubMenuItem).getPath());
    }

    @Override
    public LinearLayoutManager getHorizontalLinearLayoutManager() {
        return new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
    }

    @Override
    public View.OnClickListener createBackClickListener(Bitmap bitmap){
        return v -> mPicture.setBackgroundBitmap(bitmap);
    }

    @Override
    public View.OnClickListener createGlareClickListener(Bitmap bitmap, FrameLayout frameLayout){
        return v -> mPicture.addGlare(mImageHelper.createGlareImageView(bitmap, frameLayout, mContext));
    }

    @Override
    public View.OnClickListener createObjectClickListener(Bitmap bitmap, FrameLayout frameLayout){
        return v -> mPicture.addObject(mImageHelper.createObjectImageView(bitmap, frameLayout, mContext));
    }
}

package my.xeewi.membrane.ui.templateEditor;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import my.xeewi.membrane.common.BasePresenter;
import my.xeewi.membrane.data.PhotoInTemplate;
import my.xeewi.membrane.data.PictureTemplate;
import my.xeewi.membrane.helpers.DimensionUtils;
import my.xeewi.membrane.helpers.GalleryHelper;
import my.xeewi.membrane.helpers.ImageConstants;
import my.xeewi.membrane.helpers.ImageHelper;
import my.xeewi.membrane.helpers.TemplateHelper;

public class TemplateEditorPresenter extends BasePresenter implements TemplateEditorContract.Presenter {

    private final TemplateEditorContract.View mView;

    public FrameLayout mSelectedFrameLayout;

    private final PictureTemplate mPictureTemplate = TemplateHelper.getInstance().getPictureTemplate();

    private final Activity mContext;

    private final ImageHelper mImageHelper = ImageHelper.getInstance();

    private final GalleryHelper mGalleryHelper = GalleryHelper.getInstance();

    public TemplateEditorPresenter(@NonNull Activity context, @NonNull TemplateEditorContract.View view) {
        this.mView = view;
        this.mContext = context;
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.setPictureLayout(getPictureLayout());

        mView.setBackground(mPictureTemplate.getBackgroundImageView());

        mView.setForeground(mPictureTemplate.getForegroundImageList());

        mView.setPhotosLayout(getPhotosLayout());
    }

    public void savePhoto() {
        List<PhotoInTemplate> photoList = mPictureTemplate.getPhotoList();

        Bitmap bitmap = mImageHelper.combineImages(photoList, mPictureTemplate.getForegroundImageList());

        mGalleryHelper.saveImageToGallery(mContext, bitmap);
    }

    private ViewGroup.LayoutParams getPictureLayout(){
        int width = (int) DimensionUtils.pxToDp(mContext, DimensionUtils.getValidPx(ImageConstants.ImageWidth));
        int height = (int) DimensionUtils.pxToDp(mContext, DimensionUtils.getValidPx(ImageConstants.ImageHeight));

        return new ConstraintLayout.LayoutParams(width, height);
    }

    private List<FrameLayout> getPhotosLayout(){
        List<FrameLayout> frameLayouts = new ArrayList<>();
        for(FrameLayout frameLayout: mPictureTemplate.getPhotoFrameMap().keySet()){
            frameLayout.setOnClickListener(view -> {
                mSelectedFrameLayout = frameLayout;

                mGalleryHelper.startChooseImage(mContext, GalleryHelper.GALLERY_REQUEST);

                hidePossibilityAddPhoto(frameLayout);
            });

            frameLayouts.add(frameLayout);
        }

        return frameLayouts;
    }



    private void hidePossibilityAddPhoto(FrameLayout frameLayout){
        frameLayout.getBackground().setAlpha(0);
    }


    @Override
    public void stop() {

    }

    public FrameLayout getSelectedFrameLayout() {
        return mSelectedFrameLayout;
    }

    @Override
    public void addPhotoToPicture(Bitmap bitmap){
        FrameLayout frameLayout = getSelectedFrameLayout();

        PhotoInTemplate photo = mImageHelper.createPhotoInTemplateForPicture(mContext, bitmap, frameLayout, Objects.requireNonNull(mPictureTemplate.getPhotoFrameMap().get(frameLayout)));

        mPictureTemplate.addPhoto(photo);
    }
}

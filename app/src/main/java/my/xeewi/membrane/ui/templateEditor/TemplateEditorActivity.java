package my.xeewi.membrane.ui.templateEditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import java.io.IOException;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseActivity;
import my.xeewi.membrane.helpers.GalleryHelper;

public class TemplateEditorActivity extends BaseActivity {

    TemplateEditorContract.Presenter mPresenter;

    private boolean mIsActivityResult = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_template_editor_layout);

        TemplateEditorFragment templateEditorActivityFragment = (TemplateEditorFragment) getSupportFragmentManager()
                .findFragmentById(R.id.flContent);
        if (templateEditorActivityFragment == null) {
            templateEditorActivityFragment = TemplateEditorFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.flContent, templateEditorActivityFragment);
            transaction.commit();
        }
        mPresenter = new TemplateEditorPresenter(this, templateEditorActivityFragment);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!mIsActivityResult) {
            mPresenter.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.stop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (requestCode == GalleryHelper.GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = imageReturnedIntent.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                    mPresenter.addPhotoToPicture(bitmap);

                    mIsActivityResult = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onSavePhotoClick(View view) {
        Toast.makeText(this, R.string.photoSaved, Toast.LENGTH_SHORT).show();
        mPresenter.savePhoto();
    }
}

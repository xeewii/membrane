package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import my.xeewi.membrane.enums.MainMenuName;

public class MainMenuItem {
    private final int imageOn;
    private final int imageOff;
    private final MainMenuName mainMenuName;

    public MainMenuItem(int imageOff, int imageOn, MainMenuName mainMenuName) {
        this.imageOff = imageOff;
        this.imageOn = imageOn;
        this.mainMenuName = mainMenuName;
    }

    public MainMenuName getMainMenuName() {
        return mainMenuName;
    }

    public int getImageOff() {
        return imageOff;
    }

    public int getImageOn() {
        return imageOn;
    }
}

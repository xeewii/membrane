package my.xeewi.membrane.ui.templatesList;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import my.xeewi.membrane.common.BasePresenter;
import my.xeewi.membrane.data.PictureTemplate;
import my.xeewi.membrane.helpers.TemplateHelper;
import my.xeewi.membrane.helpers.TemplatesHelper;
import my.xeewi.membrane.ui.templateEditor.TemplateEditorActivity;

public class TemplatesListPresenter extends BasePresenter implements TemplatesListContract.Presenter {

    private final TemplatesListContract.View mView;

    public TemplatesListPresenter(@NonNull Context context, @NonNull TemplatesListContract.View view) {
        this.mView = view;
        this.mContext = context;
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.setTemplates(TemplatesHelper.getInstance().createTemplates(mContext));
    }

    @Override
    public void stop() {
    }

    @Override
    public void loadTemplateEditor(PictureTemplate pictureTemplate) {
        TemplateHelper.getInstance().setPictureTemplate(pictureTemplate);

        Intent intent = new Intent(mContext, TemplateEditorActivity.class);
        mContext.startActivity(intent);
    }
}

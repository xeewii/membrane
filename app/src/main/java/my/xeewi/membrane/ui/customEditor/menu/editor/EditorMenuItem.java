package my.xeewi.membrane.ui.customEditor.menu.editor;

import android.graphics.Bitmap;

import my.xeewi.membrane.enums.EditorName;
import my.xeewi.membrane.enums.MainMenuName;
import my.xeewi.membrane.ui.customEditor.menu.subMenu.SubMenuItem;

public class EditorMenuItem extends SubMenuItem {
    private final EditorName mEditorName;

    public EditorMenuItem(Bitmap bitmap, EditorName editorName) {
        super(bitmap, MainMenuName.EDITOR);
        mEditorName = editorName;
    }

    public EditorName getEditorName() {
        return mEditorName;
    }
}

package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import android.content.Context;

import androidx.annotation.NonNull;

import my.xeewi.membrane.common.BasePresenter;
import my.xeewi.membrane.ui.customEditor.menu.MenuController;

public class MainMenuPresenter extends BasePresenter implements MainMenuContract.Presenter {

    private final MainMenuContract.View mView;

    private final Context mContext;

    private final MainMenu mMainMenu;

    private final MenuController mMenuController = MenuController.getInstance();

    public MainMenuPresenter(@NonNull Context context, @NonNull MainMenuContract.View view) {
        this.mView = view;
        this.mContext = context;
        this.mView.setPresenter(this);

        mMainMenu = new MainMenu();
    }

    @Override
    public void start() {
        MainMenuListAdapter adapter = new MainMenuListAdapter(mView, mMainMenu.getItemList());

        mView.updateMainMenu(adapter);
    }

    @Override
    public void stop() {
    }

    @Override
    public void updateMenuController(MainMenuItem mainMenuItem){
        mMenuController.setSelectedMenuName(mainMenuItem.getMainMenuName());
    }
}

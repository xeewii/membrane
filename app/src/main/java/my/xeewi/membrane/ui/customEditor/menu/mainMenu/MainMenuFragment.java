package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Objects;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseFragment;
import my.xeewi.membrane.ui.customEditor.CustomEditorActivity;

public final class MainMenuFragment extends BaseFragment implements MainMenuContract.View {

    private MainMenuContract.Presenter mPresenter;

    public MainMenuFragment() {
    }

    public static MainMenuFragment newInstance() {
        return new MainMenuFragment();
    }

    @Override
    public void setPresenter(MainMenuContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_menu_layout, container, false);
    }

    @Override
    public void updateMainMenu(MainMenuListAdapter adapter){
        RecyclerView recyclerView = Objects.requireNonNull(getView()).findViewById(R.id.rvMainMenuList);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loadSubMenu(MainMenuItem mainMenuItem) {
        mPresenter.updateMenuController(mainMenuItem);

        ((CustomEditorActivity) Objects.requireNonNull(getActivity())).showSubMenuFragment();
    }
}

package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.graphics.Bitmap;
import android.widget.FrameLayout;

import androidx.recyclerview.widget.LinearLayoutManager;

import my.xeewi.membrane.common.BaseContract;
import my.xeewi.membrane.ui.customEditor.menu.editor.EditorMenuItem;

public interface SubMenuContract {

    interface View extends BaseContract.View<Presenter> {
        void updateSubMenu(SubMenuListAdapter adapter);

        void loadSubMenuForCategory(CategorySubMenuItem categorySubMenuItem);

        android.view.View.OnClickListener getBackClickListener(Bitmap bitmap);

        android.view.View.OnClickListener getGlareClickListener(Bitmap bitmap);

        android.view.View.OnClickListener getObjectClickListener(Bitmap bitmap);

        android.view.View.OnClickListener getEditorClickListener(EditorMenuItem editorMenuItem);
    }

    interface Presenter extends BaseContract.Presenter {
        void updateItemList();

        void updateMenuController(CategorySubMenuItem categorySubMenuItem);

        LinearLayoutManager getHorizontalLinearLayoutManager();

        android.view.View.OnClickListener createBackClickListener(Bitmap bitmap);

        android.view.View.OnClickListener createGlareClickListener(Bitmap bitmap, FrameLayout frameLayout);

        android.view.View.OnClickListener createObjectClickListener(Bitmap bitmap, FrameLayout frameLayout);
    }
}

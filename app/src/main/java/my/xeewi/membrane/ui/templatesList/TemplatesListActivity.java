package my.xeewi.membrane.ui.templatesList;

import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseActivity;

public class TemplatesListActivity extends BaseActivity {

    TemplatesListContract.Presenter mPresenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_templates_list_layout);

        TemplatesListFragment templatesListFragment = (TemplatesListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.flContent);
        if (templatesListFragment == null) {
            templatesListFragment = TemplatesListFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.flContent, templatesListFragment);
            transaction.commit();
        }
        mPresenter = new TemplatesListPresenter(this, templatesListFragment);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.stop();
    }
}

package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import my.xeewi.membrane.R;

public class MainMenuListAdapter extends RecyclerView.Adapter<MainMenuViewHolder> {
    private final List<MainMenuItem> mMainMenuItems;
    private final MainMenuContract.View mView;
    private final List<MainMenuViewHolder> mHolders = new ArrayList<>();

    public MainMenuListAdapter(MainMenuContract.View view, List<MainMenuItem> mainMenuItems) {
        mMainMenuItems = mainMenuItems;
        mView = view;
    }

    @NonNull
    @Override
    public MainMenuViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_menu_item, null);
        MainMenuViewHolder mainMenuViewHolder = new MainMenuViewHolder(view, mView, this);
        mHolders.add(mainMenuViewHolder);
        return mainMenuViewHolder;
    }

    @Override
    public void onBindViewHolder(MainMenuViewHolder holder, int position) {
        MainMenuItem mainMenuItem = mMainMenuItems.get(position);
        holder.onBind(mainMenuItem);
    }

    @Override
    public int getItemCount() {
        return (null != mMainMenuItems ? mMainMenuItems.size() : 0);
    }

    public void makeMainMenuInactive(){
        for(int i=0; i<mHolders.size(); i++){
            mHolders.get(i).setBackground(mMainMenuItems.get(i).getImageOff());
        }
    }
}
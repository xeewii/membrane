package my.xeewi.membrane.ui.customEditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import java.io.IOException;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseActivity;
import my.xeewi.membrane.common.BaseContract;
import my.xeewi.membrane.helpers.GalleryHelper;
import my.xeewi.membrane.ui.customEditor.menu.editor.EditorSeekBarFragment;
import my.xeewi.membrane.ui.customEditor.menu.editor.EditorSeekBarPresenter;
import my.xeewi.membrane.ui.customEditor.menu.mainMenu.MainMenuFragment;
import my.xeewi.membrane.ui.customEditor.menu.mainMenu.MainMenuPresenter;
import my.xeewi.membrane.ui.customEditor.menu.subMenu.SubMenuFragment;
import my.xeewi.membrane.ui.customEditor.menu.subMenu.SubMenuPresenter;

public class CustomEditorActivity extends BaseActivity {
    CustomEditorContract.Presenter mPresenter;

    private MainMenuFragment mMainMenuFragment;
    private SubMenuFragment mSubMenuFragment;
    private EditorSeekBarFragment mEditorSeekBarFragment;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_custom_editor_layout);

        showCustomEditorFragment();

        showMainMenuFragment();

        GalleryHelper.getInstance().startChooseImage(this, GalleryHelper.GALLERY_REQUEST);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.stop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (requestCode == GalleryHelper.GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = imageReturnedIntent.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);

                    mPresenter.addPhotoToPicture(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showCustomEditorFragment(){
        CustomEditorFragment customEditorFragment = (CustomEditorFragment) getSupportFragmentManager()
                .findFragmentById(R.id.flContent);
        if (customEditorFragment == null) {
            customEditorFragment = CustomEditorFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.flContent, customEditorFragment);
            transaction.commit();
        }

        mPresenter = new CustomEditorPresenter(this, customEditorFragment);
    }

    public void showMainMenuFragment(){
        mMainMenuFragment = (MainMenuFragment) getSupportFragmentManager().findFragmentById(R.id.flMainMenu);
        if (mMainMenuFragment == null) {
            mMainMenuFragment = MainMenuFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.flMainMenu, mMainMenuFragment);
            transaction.commit();
        }

        mPresenter.setMainMenuPresenter(new MainMenuPresenter(this, mMainMenuFragment));
    }

    public void showSubMenuFragment(){
        boolean isExistsFragment = false;
        mSubMenuFragment = (SubMenuFragment) getSupportFragmentManager().findFragmentById(R.id.flSubMenu);
        if (mSubMenuFragment == null) {
            mSubMenuFragment = SubMenuFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.flSubMenu, mSubMenuFragment);
            transaction.commit();
        }
        else{
            isExistsFragment = true;
        }

        SubMenuPresenter subMenuPresenter = new SubMenuPresenter(this, mSubMenuFragment);
        subMenuPresenter.start();

        if(isExistsFragment) {
            subMenuPresenter.updateItemList();
        }
    }

    public void showSeekBarFragment(){
        mEditorSeekBarFragment = (EditorSeekBarFragment) getSupportFragmentManager().findFragmentById(R.id.flEditorSeekBar);
        if (mEditorSeekBarFragment == null) {
            mEditorSeekBarFragment = EditorSeekBarFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.hide(mMainMenuFragment);
            transaction.hide(mSubMenuFragment);
            transaction.add(R.id.flEditorSeekBar, mEditorSeekBarFragment);
            transaction.commit();
        }
        else{
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.hide(mMainMenuFragment);
            transaction.hide(mSubMenuFragment);
            transaction.show(mEditorSeekBarFragment);
            transaction.commit();
        }

        EditorSeekBarPresenter editorSeekBarPresenter = new EditorSeekBarPresenter(this, mEditorSeekBarFragment);
        editorSeekBarPresenter.start();
    }

    public void hideSeekBarFragment(){
       FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
       transaction.show(mMainMenuFragment);
       transaction.show(mSubMenuFragment);
       transaction.hide(mEditorSeekBarFragment);
       transaction.commit();
    }

    public void onSavePhotoClick(View view) {
        Toast.makeText(this, R.string.photoSaved, Toast.LENGTH_SHORT).show();
        mPresenter.savePhoto();
    }

    public void onAddPhotoClick(View view) {
        GalleryHelper.getInstance().startChooseImage(this, GalleryHelper.GALLERY_REQUEST);
    }
}

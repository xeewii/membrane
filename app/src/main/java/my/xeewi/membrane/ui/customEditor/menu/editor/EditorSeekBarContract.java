package my.xeewi.membrane.ui.customEditor.menu.editor;

import android.widget.SeekBar;

import my.xeewi.membrane.common.BaseContract;

public class EditorSeekBarContract {

    public interface View extends BaseContract.View<EditorSeekBarContract.Presenter> {
        void hideFragment();

        void tuneSeekBar();
    }

    public interface Presenter extends BaseContract.Presenter {
        SeekBar.OnSeekBarChangeListener getSeekBarOnClickListener();

        android.view.View.OnClickListener getCancelButtonOnClickListener();

        android.view.View.OnClickListener getOkButtonOnClickListener();

        int getMaxValueForBar();

        int getDefaultValueForBar();
    }
}

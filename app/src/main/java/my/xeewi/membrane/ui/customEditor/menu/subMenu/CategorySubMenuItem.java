package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.graphics.Bitmap;

import my.xeewi.membrane.enums.MainMenuName;

public class CategorySubMenuItem extends SubMenuItem {
    private final String mPath;
    private final String mName;

    public CategorySubMenuItem(String path, Bitmap previewBitmap, String name, MainMenuName mainMenuName){
        super(previewBitmap, mainMenuName);

        mPath = path;
        mName = name;
    }

    public String getPath() {
        return mPath;
    }

    @Override
    public String getName() {
        return mName;
    }
}
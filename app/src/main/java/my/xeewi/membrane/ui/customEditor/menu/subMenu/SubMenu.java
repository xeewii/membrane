package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.List;

import my.xeewi.membrane.enums.EditorName;
import my.xeewi.membrane.enums.MainMenuName;
import my.xeewi.membrane.helpers.ImageHelper;
import my.xeewi.membrane.ui.customEditor.menu.MenuController;
import my.xeewi.membrane.ui.customEditor.menu.editor.EditorMenuItem;

public class SubMenu {
    private final List<IMenuItem> itemList = new ArrayList<>();

    private final Context mContext;

    private final ImageHelper mImageHelper = ImageHelper.getInstance();

    public SubMenu(Context context){
        mContext = context;
    }

    public void initializeObjectCategoryMenu(){
        addObjectsCategoryItem("object/smile", "Smile");
        addObjectsCategoryItem("object/cat", "Cat");
        addObjectsCategoryItem("object/anime", "Anime");
        addObjectsCategoryItem("object/flower", "Flower");
        addObjectsCategoryItem("object/dragon", "Dragon");
        addObjectsCategoryItem("object/heart", "Heart");
        addObjectsCategoryItem("object/music", "Music");
        addObjectsCategoryItem("object/sweet", "Sweet");
        addObjectsCategoryItem("object/tattoo", "Tattoo");
    }

    private void addObjectsCategoryItem(String path, String name){
        addCategoryItem(path, name, MainMenuName.OBJECTS);
    }

    private void initializeMenu(String path, MainMenuName mainMenuName) {
        List<Bitmap> bitmapList = mImageHelper.loadFolder(mContext, path);
        for (Bitmap bitmap : bitmapList) {
            addItem(new SubMenuItem(bitmap, mainMenuName));
        }
    }

    private void initializeEditorMenu(){
        String path = "editor/";
        addEditorMenuItem(path + "exposition.png", EditorName.EXPOSITION);
        addEditorMenuItem(path + "saturation.png", EditorName.SATURATION);
        addEditorMenuItem(path + "contrast.png", EditorName.CONTRAST);
        addEditorMenuItem(path + "temperature.png", EditorName.TEMPERATURE);
        addEditorMenuItem(path + "hue.png", EditorName.HUE);
    }

    private void addEditorMenuItem(String path, EditorName editorName){
        Bitmap bitmap = mImageHelper.loadImage(mContext, path);
        addItem(new EditorMenuItem(bitmap, editorName));
    }

    private void initializeBackCategoryMenu(){
        addBackCategoryItem("back/brick", "Brick");
        addBackCategoryItem("back/cyber", "Cyber");
        addBackCategoryItem("back/nature", "Nature");
        addBackCategoryItem("back/news", "News");
        addBackCategoryItem("back/paper", "Paper");
        addBackCategoryItem("back/space", "Space");
    }

    private void addBackCategoryItem(String path, String name){
        addCategoryItem(path, name, MainMenuName.BACK);
    }

    private void addCategoryItem(String path, String name, MainMenuName mainMenuName){
        Bitmap bitmap = mImageHelper.loadImage(mContext, path+"/preview.png");
        addItem(new CategorySubMenuItem(path, bitmap, name, mainMenuName));
    }

    public void initializeGlareCategoryMenu(){
        addGlareCategoryItem("glare/glare_1", "glare_1");
        addGlareCategoryItem("glare/glare_2", "glare_2");
        addGlareCategoryItem("glare/glare_3", "glare_3");
    }

    private void addGlareCategoryItem(String path, String name){
        addCategoryItem(path, name, MainMenuName.GLARE);
    }

    private void addItem(IMenuItem item){
        itemList.add(item);
    }

    public List<IMenuItem> getItemList(){
        itemList.clear();

        MenuController menuController = MenuController.getInstance();

        MainMenuName menuName = menuController.getSelectedMenuName();
        String path = menuController.getPath();

        switch (menuName) {
            case EDITOR: {
                initializeEditorMenu();
                break;
            }
            case BACK:{
                initializeMenu(path, MainMenuName.BACK);
                break;
            }
            case GLARE:{
                initializeMenu(path, MainMenuName.GLARE);
                break;
            }
            case OBJECTS: {
                initializeMenu(path, MainMenuName.OBJECTS);
                break;
            }
            case BACK_CATEGORY: {
                initializeBackCategoryMenu();
                break;
            }
            case GLARE_CATEGORY: {
                initializeGlareCategoryMenu();
                break;
            }
            case OBJECTS_CATEGORY: {
                initializeObjectCategoryMenu();
                break;
            }
        }

        return itemList;
    }
}

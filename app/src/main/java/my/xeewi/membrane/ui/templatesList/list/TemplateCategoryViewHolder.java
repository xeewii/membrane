package my.xeewi.membrane.ui.templatesList.list;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import my.xeewi.membrane.R;
import my.xeewi.membrane.data.TemplateCategory;
import my.xeewi.membrane.ui.templatesList.TemplatesListContract;

public class TemplateCategoryViewHolder extends RecyclerView.ViewHolder {
    private final TextView mItemTitle;
    private final RecyclerView mTemplateListRecyclerView;
    private final TemplatesListContract.View mView;

    public TemplateCategoryViewHolder(View viewHolder, TemplatesListContract.View view) {
        super(viewHolder);

        mView = view;
        mItemTitle = viewHolder.findViewById(R.id.tvCategoryTemplatesName);

        mTemplateListRecyclerView = viewHolder.findViewById(R.id.rvTemplateList);
        mTemplateListRecyclerView.setHasFixedSize(true);
        mTemplateListRecyclerView.setLayoutManager(mView.getHorizontalLinearLayoutManager());
    }

    public void onBind(TemplateCategory templateCategory){
        mItemTitle.setText(templateCategory.getNameCategory());

        TemplateListAdapter itemListDataAdapter = new TemplateListAdapter(mView, templateCategory.getTemplateList());
        mTemplateListRecyclerView.setAdapter(itemListDataAdapter);
    }
}
package my.xeewi.membrane.ui.customEditor;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import java.util.List;

import my.xeewi.membrane.common.BasePresenter;
import my.xeewi.membrane.data.Photo;
import my.xeewi.membrane.data.Picture;
import my.xeewi.membrane.helpers.DimensionUtils;
import my.xeewi.membrane.helpers.GalleryHelper;
import my.xeewi.membrane.helpers.ImageConstants;
import my.xeewi.membrane.helpers.ImageHelper;
import my.xeewi.membrane.ui.customEditor.menu.mainMenu.MainMenuContract;
import my.xeewi.membrane.ui.customEditor.menu.mainMenu.MainMenuPresenter;

public class CustomEditorPresenter extends BasePresenter implements CustomEditorContract.Presenter {

    private final CustomEditorContract.View mView;

    private MainMenuContract.Presenter mMainMenuPresenter;

    private final Picture mPicture = Picture.getInstance();

    private final ImageHelper mImageHelper = ImageHelper.getInstance();

    private final GalleryHelper mGalleryHelper = GalleryHelper.getInstance();

    public CustomEditorPresenter(@NonNull Context context, @NonNull CustomEditorContract.View view) {
        this.mView = view;
        this.mContext = context;
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        mMainMenuPresenter.start();

        mView.setBackground(createBackgroundImageView());

        mView.setPictureFrameLayout(getPictureLayout());
    }

    @Override
    public void stop() {

    }

    private ViewGroup.LayoutParams getPictureLayout(){
        int width = (int) DimensionUtils.pxToDp(mContext, DimensionUtils.getValidPx(ImageConstants.ImageWidth));
        int height = (int) DimensionUtils.pxToDp(mContext, DimensionUtils.getValidPx(ImageConstants.ImageHeight));

        return new ViewGroup.LayoutParams(width, height);
    }

    @Override
    public void setMainMenuPresenter(MainMenuPresenter mainMenuPresenter){
        mMainMenuPresenter = mainMenuPresenter;
    }

    @Override
    public void addPhotoToPicture(Bitmap bitmap){
        Photo photo = mImageHelper.createPhotoForPicture(mContext, mView.getFrameLayoutForPhotos(), bitmap);

        mPicture.addPhoto(photo);
        mPicture.setPhotoSelected(photo);
    }

    @Override
    public void savePhoto() {
        List<Photo> photoList = Picture.getInstance().getPhotoList();

        Bitmap bitmap = mImageHelper.combineImages(photoList);

        ImageView imageView = new ImageView(mContext);
        imageView.setImageBitmap(bitmap);

        mGalleryHelper.saveImageToGallery(mContext, bitmap);
    }

    @Override
    public void setBackgroundImageView(ImageView imageView) {
        mPicture.setBackgroundImageView(imageView);
    }

    private ImageView createBackgroundImageView(){
        Bitmap bitmap = mImageHelper.loadImage(mContext,"texture_test.jpg");

        ImageView imageView = new ImageView(mContext);

        imageView.setImageBitmap(bitmap);

        return imageView;
    }
}

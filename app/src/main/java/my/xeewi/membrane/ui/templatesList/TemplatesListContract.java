package my.xeewi.membrane.ui.templatesList;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import my.xeewi.membrane.common.BaseContract;
import my.xeewi.membrane.data.PictureTemplate;
import my.xeewi.membrane.data.Template;
import my.xeewi.membrane.data.TemplateCategory;

public interface TemplatesListContract {

    interface View extends BaseContract.View<Presenter> {
        void setTemplates(List<TemplateCategory> templateCategoryList);

        void loadTemplate(Template template);

        LinearLayoutManager getHorizontalLinearLayoutManager();
    }

    interface Presenter extends BaseContract.Presenter {
        void loadTemplateEditor(PictureTemplate pictureTemplate);
    }
}

package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.view.View;
import android.widget.TextView;

import my.xeewi.membrane.R;

public class CategorySubMenuViewHolder extends AMenuViewHolder {
    private final SubMenuContract.View mView;

    private final TextView mTextView;

    public CategorySubMenuViewHolder(View viewHolder, SubMenuContract.View view) {
        super(viewHolder);

        mView = view;

        mImageView = viewHolder.findViewById(R.id.ivPreview);
        mLinearLayout = viewHolder.findViewById(R.id.linear_layout);
        mTextView = viewHolder.findViewById(R.id.tvName);
    }

    public void onBind(IMenuItem menuItem){
        mImageView.setImageBitmap(menuItem.getPreviewBitmap());

        mTextView.setText(menuItem.getName());

        mLinearLayout.setOnClickListener(v -> mView.loadSubMenuForCategory((CategorySubMenuItem) menuItem));
    }
}
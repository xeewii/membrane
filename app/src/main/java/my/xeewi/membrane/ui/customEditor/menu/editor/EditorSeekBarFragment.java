package my.xeewi.membrane.ui.customEditor.menu.editor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseFragment;
import my.xeewi.membrane.ui.customEditor.CustomEditorActivity;

public final class EditorSeekBarFragment extends BaseFragment implements EditorSeekBarContract.View {

    private EditorSeekBarContract.Presenter mPresenter;

    private SeekBar mSeekBar;

    public EditorSeekBarFragment() {
    }

    public static EditorSeekBarFragment newInstance() {
        return new EditorSeekBarFragment();
    }

    @Override
    public void setPresenter(EditorSeekBarContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_editor_seek_bar, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        tuneFragment();
    }

    public void tuneFragment() {
        mSeekBar = getView().findViewById(R.id.seek_bar);

        mSeekBar.setOnSeekBarChangeListener(mPresenter.getSeekBarOnClickListener());

        tuneSeekBar();

        Button cancelButton = getView().findViewById(R.id.bCancel);
        cancelButton.setOnClickListener(mPresenter.getCancelButtonOnClickListener());

        Button okButton = getView().findViewById(R.id.bOk);
        okButton.setOnClickListener(mPresenter.getOkButtonOnClickListener());
    }

    @Override
    public void tuneSeekBar() {
        if(mSeekBar != null) {
            mSeekBar.setMax(mPresenter.getMaxValueForBar());
            mSeekBar.setProgress(mPresenter.getDefaultValueForBar());
        }
    }

    @Override
    public void hideFragment(){
        ((CustomEditorActivity)getActivity()).hideSeekBarFragment();
    }
}

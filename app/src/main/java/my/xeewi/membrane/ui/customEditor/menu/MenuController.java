package my.xeewi.membrane.ui.customEditor.menu;

import my.xeewi.membrane.enums.MainMenuName;

public class MenuController {
    private MainMenuName mSelectedMenuName = MainMenuName.SKINS;
    private String mPath;

    public MainMenuName getSelectedMenuName() {
        return mSelectedMenuName;
    }

    private static class MenuControllerHolder {
        private final static MenuController instance = new MenuController();
    }

    public static MenuController getInstance(){
        return MenuController.MenuControllerHolder.instance;
    }

    public void setSelectedMenuName(MainMenuName selectedMenuName) {
        mSelectedMenuName = selectedMenuName;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }
}

package my.xeewi.membrane.ui.customEditor.menu.editor;

import android.content.Context;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.NonNull;

import my.xeewi.membrane.common.BasePresenter;
import my.xeewi.membrane.helpers.EditorHelper;

public class EditorSeekBarPresenter extends BasePresenter implements EditorSeekBarContract.Presenter {

    private final EditorSeekBarContract.View mView;

    private final EditorHelper mEditorHelper = EditorHelper.getInstance();

    public EditorSeekBarPresenter(@NonNull Context context, @NonNull EditorSeekBarContract.View view) {
        mView = view;
        mContext = context;
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.tuneSeekBar();
    }

    @Override
    public void stop() {

    }

    @Override
    public SeekBar.OnSeekBarChangeListener getSeekBarOnClickListener(){
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mEditorHelper.editImage(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
    }

    @Override
    public View.OnClickListener getCancelButtonOnClickListener(){
        return v -> {
            mEditorHelper.cancelChanges();

            mView.hideFragment();
        };
    }

    @Override
    public View.OnClickListener getOkButtonOnClickListener(){
        return v -> {
            mEditorHelper.saveChanges();

            mView.hideFragment();
        };
    }

    @Override
    public int getMaxValueForBar(){
        return mEditorHelper.getMaxValueForBar();
    }

    @Override
    public int getDefaultValueForBar(){
        return mEditorHelper.getDefaultValueForBar();
    }
}

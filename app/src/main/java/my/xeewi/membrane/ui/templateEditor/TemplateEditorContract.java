package my.xeewi.membrane.ui.templateEditor;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.List;

import my.xeewi.membrane.common.BaseContract;
import my.xeewi.membrane.data.ForegroundImage;

public interface TemplateEditorContract {

    interface View extends BaseContract.View<Presenter> {
        void setPhotosLayout(List<FrameLayout> layouts);

        void setPictureLayout(ViewGroup.LayoutParams layoutParams);

        void setForeground(List<ForegroundImage> foregroundImages);

        void setBackground(ImageView background);
    }

    interface Presenter extends BaseContract.Presenter {
        void addPhotoToPicture(Bitmap bitmap);

        void savePhoto();
    }
}

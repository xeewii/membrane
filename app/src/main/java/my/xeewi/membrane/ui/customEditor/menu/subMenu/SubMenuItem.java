package my.xeewi.membrane.ui.customEditor.menu.subMenu;
import android.graphics.Bitmap;

import my.xeewi.membrane.enums.MainMenuName;

public class SubMenuItem implements IMenuItem {
    private final Bitmap mPreviewBitmap;
    private final MainMenuName mMainMenuName;

    public SubMenuItem(Bitmap previewBitmap, MainMenuName mainMenuName) {
        mPreviewBitmap = previewBitmap;
        mMainMenuName = mainMenuName;
    }

    @Override
    public Bitmap getPreviewBitmap() {
        return mPreviewBitmap;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public MainMenuName getMainMenuName() {
        return mMainMenuName;
    }
}


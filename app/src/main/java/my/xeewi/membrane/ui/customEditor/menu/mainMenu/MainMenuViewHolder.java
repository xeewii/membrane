package my.xeewi.membrane.ui.customEditor.menu.mainMenu;

import android.view.View;
import android.widget.Button;
import androidx.recyclerview.widget.RecyclerView;

import my.xeewi.membrane.R;

public class MainMenuViewHolder extends RecyclerView.ViewHolder {
    private final Button button;
    private final MainMenuContract.View mView;
    private final MainMenuListAdapter mMainMenuListAdapter;

    public MainMenuViewHolder(View viewHolder, MainMenuContract.View view, MainMenuListAdapter adapter) {
        super(viewHolder);

        mView = view;
        mMainMenuListAdapter = adapter;
        button = viewHolder.findViewById(R.id.bMainMenu);
    }

    public void onBind(MainMenuItem mainMenuItem){
        button.setBackgroundResource(mainMenuItem.getImageOff());

        button.setOnClickListener(v -> {
            mMainMenuListAdapter.makeMainMenuInactive();

            setBackground(mainMenuItem.getImageOn());

            mView.loadSubMenu(mainMenuItem);
        });
    }

    public void setBackground(int resource){
        button.setBackgroundResource(resource);
    }
}
package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Objects;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseFragment;
import my.xeewi.membrane.helpers.EditorHelper;
import my.xeewi.membrane.ui.customEditor.CustomEditorActivity;
import my.xeewi.membrane.ui.customEditor.menu.editor.EditorMenuItem;

public final class SubMenuFragment extends BaseFragment implements SubMenuContract.View {

    private SubMenuContract.Presenter mPresenter;

    private final EditorHelper mEditorHelper = EditorHelper.getInstance();

    public SubMenuFragment() {
    }

    public static SubMenuFragment newInstance() {
        return new SubMenuFragment();
    }

    @Override
    public void setPresenter(SubMenuContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sub_menu_layout, container, false);
    }
 
    @Override
    public void onStart() {
        super.onStart();

        mPresenter.updateItemList();
    }

    @Override
    public void updateSubMenu(SubMenuListAdapter adapter){
        RecyclerView recyclerView = Objects.requireNonNull(getView()).findViewById(R.id.rvSubMenuList);

        recyclerView.setLayoutManager(mPresenter.getHorizontalLinearLayoutManager());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loadSubMenuForCategory(CategorySubMenuItem categorySubMenuItem){
        mPresenter.updateMenuController(categorySubMenuItem);

        mPresenter.updateItemList();
    }

    @Override
    public View.OnClickListener getBackClickListener(Bitmap bitmap){
        return mPresenter.createBackClickListener(bitmap);
    }

    @Override
    public View.OnClickListener getGlareClickListener(Bitmap bitmap){
        FrameLayout frameLayout = Objects.requireNonNull(getActivity()).findViewById(R.id.flPhotos);

        return mPresenter.createGlareClickListener(bitmap, frameLayout);
    }

    @Override
    public View.OnClickListener getObjectClickListener(Bitmap bitmap){
        FrameLayout frameLayout = Objects.requireNonNull(getActivity()).findViewById(R.id.flPhotos);

        return mPresenter.createObjectClickListener(bitmap, frameLayout);
    }

    @Override
    public View.OnClickListener getEditorClickListener(EditorMenuItem editorMenuItem){
        return v -> {
            mEditorHelper.setSelectedEditorName(editorMenuItem.getEditorName());
            ((CustomEditorActivity) Objects.requireNonNull(getActivity())).showSeekBarFragment();
        };
    }
}

package my.xeewi.membrane.ui.customEditor.menu.subMenu;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import my.xeewi.membrane.R;
import my.xeewi.membrane.enums.MainMenuName;
import my.xeewi.membrane.ui.customEditor.menu.MenuController;

public class SubMenuListAdapter extends RecyclerView.Adapter<AMenuViewHolder> {
    private final List<IMenuItem> mSubMenuItems;
    private final SubMenuContract.View mView;

    public SubMenuListAdapter(SubMenuContract.View view, List<IMenuItem> subMenuItems) {
        mSubMenuItems = subMenuItems;
        mView = view;
    }

    @NonNull
    @Override
    public AMenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MainMenuName menuName = MenuController.getInstance().getSelectedMenuName();

        int resource = 0;

        switch (menuName) {
            case BACK_CATEGORY:
            case GLARE_CATEGORY:
            case OBJECTS_CATEGORY: {
                resource = R.layout.category_sub_menu_item;
                break;
            }
            case EDITOR:
            case BACK:
            case GLARE:
            case OBJECTS:
            case SKINS: {
                resource = R.layout.sub_menu_item;
            }
        }

        @SuppressLint("InflateParams") View view = LayoutInflater.from(viewGroup.getContext()).inflate(resource, null);

        switch (menuName) {
            case BACK_CATEGORY:
            case GLARE_CATEGORY:
            case OBJECTS_CATEGORY: {
                return new CategorySubMenuViewHolder(view, mView);
            }
            case EDITOR:
            case BACK:
            case GLARE:
            case OBJECTS:
            case SKINS: {
                return new SubMenuViewHolder(view, mView);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(AMenuViewHolder holder, int position) {
        IMenuItem menuItem = mSubMenuItems.get(position);
        holder.onBind(menuItem);
    }

    @Override
    public int getItemCount() {
        return (null != mSubMenuItems ? mSubMenuItems.size() : 0);
    }
}
package my.xeewi.membrane.ui.templatesList.list;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import my.xeewi.membrane.R;
import my.xeewi.membrane.data.Template;
import my.xeewi.membrane.ui.templatesList.TemplatesListContract;

public class TemplateListAdapter extends RecyclerView.Adapter<TemplateViewHolder> {
    private final ArrayList<Template> mTemplateList;
    private final TemplatesListContract.View mView;

    public TemplateListAdapter(TemplatesListContract.View view, ArrayList<Template> templateList) {
        mTemplateList = templateList;
        mView = view;
    }

    @NonNull
    @Override
    public TemplateViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_item, null);
        return new TemplateViewHolder(view, mView);
    }

    @Override
    public void onBindViewHolder(TemplateViewHolder holder, int position) {
        holder.onBind(mTemplateList.get(position));
    }

    @Override
    public int getItemCount() {
        return (null != mTemplateList ? mTemplateList.size() : 0);
    }
}

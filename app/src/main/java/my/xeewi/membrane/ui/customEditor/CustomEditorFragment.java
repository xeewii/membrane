package my.xeewi.membrane.ui.customEditor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.Objects;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseFragment;

public final class CustomEditorFragment extends BaseFragment implements CustomEditorContract.View {

    private CustomEditorContract.Presenter mPresenter;

    public CustomEditorFragment() {
    }

    public static CustomEditorFragment newInstance() {
        return new CustomEditorFragment();
    }

    @Override
    public void setPresenter(CustomEditorContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_custom_editor_layout, container, false);
    }

    @Override
    public void setBackground(ImageView imageView){
        FrameLayout frameLayout = Objects.requireNonNull(getView()).findViewById(R.id.flBackground);
        frameLayout.addView(imageView);

        mPresenter.setBackgroundImageView(imageView);
    }

    @Override
    public void setPictureFrameLayout(ViewGroup.LayoutParams layoutParams){
        FrameLayout pictureFrameLayout = Objects.requireNonNull(getView()).findViewById(R.id.flPicture);

        ViewGroup.LayoutParams pictureLayoutParams = pictureFrameLayout.getLayoutParams();
        pictureLayoutParams.width = layoutParams.width;
        pictureLayoutParams.height = layoutParams.height;

        pictureFrameLayout.setLayoutParams(pictureLayoutParams);
    }

    @Override
    public FrameLayout getFrameLayoutForPhotos(){
        return Objects.requireNonNull(getView()).findViewById(R.id.flPhotos);
    }
}

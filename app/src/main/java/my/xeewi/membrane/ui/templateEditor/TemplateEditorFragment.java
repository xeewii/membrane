package my.xeewi.membrane.ui.templateEditor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.List;
import java.util.Objects;

import my.xeewi.membrane.R;
import my.xeewi.membrane.common.BaseFragment;
import my.xeewi.membrane.data.ForegroundImage;

public final class TemplateEditorFragment extends BaseFragment implements TemplateEditorContract.View {

    private TemplateEditorContract.Presenter mPresenter;

    public TemplateEditorFragment() {
    }

    public static TemplateEditorFragment newInstance() {
        return new TemplateEditorFragment();
    }

    @Override
    public void setPresenter(TemplateEditorContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_template_editor_layout, container, false);
    }

    @Override
    public void setPhotosLayout(List<FrameLayout> layouts) {
        FrameLayout photosFrameLayout = Objects.requireNonNull(getView()).findViewById(R.id.flPhotos);

        for (FrameLayout frameLayout : layouts) {
            photosFrameLayout.addView(frameLayout);
        }
    }

    @Override
    public void setPictureLayout(ViewGroup.LayoutParams layoutParams) {
        FrameLayout pictureFrameLayout = Objects.requireNonNull(getView()).findViewById(R.id.flPicture);

        ViewGroup.LayoutParams pictureLayoutParams = pictureFrameLayout.getLayoutParams();
        pictureLayoutParams.width = layoutParams.width;
        pictureLayoutParams.height = layoutParams.height;

        pictureFrameLayout.setLayoutParams(pictureLayoutParams);
    }

    @Override
    public void setForeground(List<ForegroundImage> foregroundImages) {
        FrameLayout foregroundFrameLayout = Objects.requireNonNull(getView()).findViewById(R.id.foreground_frame_layout);

        for (ForegroundImage foregroundImage: foregroundImages) {
            foregroundFrameLayout.addView(foregroundImage.getImageView());
        }
    }

    @Override
    public void setBackground(ImageView background) {
        FrameLayout backgroundFrameLayout = Objects.requireNonNull(getView()).findViewById(R.id.flBackground);
        backgroundFrameLayout.addView(background);
    }
}

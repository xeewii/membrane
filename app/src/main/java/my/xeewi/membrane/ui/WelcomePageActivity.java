package my.xeewi.membrane.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import my.xeewi.membrane.R;
import my.xeewi.membrane.ui.customEditor.CustomEditorActivity;
import my.xeewi.membrane.ui.templatesList.TemplatesListActivity;

public class WelcomePageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
    }

    public void onTemplatesButtonClick(View view) {
        Intent intent = new Intent(this, TemplatesListActivity.class);
        startActivity(intent);
    }

    public void onCustomButtonClick(View view) {
        Intent intent = new Intent(this, CustomEditorActivity.class);
        startActivity(intent);
    }
}
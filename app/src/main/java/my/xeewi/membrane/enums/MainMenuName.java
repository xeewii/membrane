package my.xeewi.membrane.enums;

public enum MainMenuName {
    EDITOR,
    BACK,
    BACK_CATEGORY,
    GLARE,
    GLARE_CATEGORY,
    OBJECTS,
    OBJECTS_CATEGORY,
    SKINS,
    FILTERS,
    EFFECTS,
    TEXT
}
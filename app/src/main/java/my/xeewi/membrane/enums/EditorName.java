package my.xeewi.membrane.enums;

public enum EditorName {
    EXPOSITION,
    SATURATION,
    CONTRAST,
    TEMPERATURE,
    HUE
}
